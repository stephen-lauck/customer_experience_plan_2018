# Gitlab Customer Experience

## Customer Metrics
  * ARR+ ranking
  * Potential Upsell
  * software tracking (gainsights?)
  * telemetry baseline / growth
  * NPS
  * QBR ( and maybe why not QBR )

## Customer Journey
  * onboarding
  * adoption
  * expansion/renewal

## Maturity Model
## Code Ecosystem
## Enterprise Readiness
  * download to first value
  * guides
  * onboarding
  * pipeline to customers
  * upgrades
  * security
  * expansion / scale

## Opensource Conversions
## Wholistic Post Sales Approach
  * Sales Engineering
  * Post Sales Engineering
  * Solutions Engineering
  * Support Engineering

## TAM / CSM
## Tactical / Strategic
